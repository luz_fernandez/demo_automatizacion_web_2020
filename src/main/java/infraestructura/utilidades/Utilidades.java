package infraestructura.utilidades;

public class Utilidades {

	public static void esperar(int segundos) {
		try {
			Thread.sleep(segundos);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
