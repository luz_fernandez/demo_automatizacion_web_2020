package ui;

import org.openqa.selenium.NoSuchElementException;

public class excepciones {

	public static void main(String[] args) {

		System.out.println("Antes de try/cath");

		try {
			System.out.println("Antes de lanzar exc");
			System.out.println("1");
			throw new NoSuchElementException("No se encontro el elemento");
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Despues de try catch");
	}

}
